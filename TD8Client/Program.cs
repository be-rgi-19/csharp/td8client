﻿using System;
using System.Net.Sockets;

namespace TD8Client
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] b_sf;
            try
            {
                Console.WriteLine("Saisir un nombre d'années");
                int nb_an = int.Parse(Console.ReadLine());
                Console.WriteLine("Saisir le placement de départ");
                Double cap_ini = Double.Parse(Console.ReadLine());

                Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                sock.Connect("localhost", 1212);
                byte[] b_an = BitConverter.GetBytes(nb_an);
                byte[] b_si = BitConverter.GetBytes(cap_ini);
                sock.Send(b_an);
                sock.Send(b_si);
                b_sf = new byte[8];
                sock.Receive(b_sf);

                sock.Shutdown(SocketShutdown.Both);
                sock.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                throw;
            }
            // décodage du tableau reçu
            double cap_fin = BitConverter.ToDouble(b_sf, 0);
            Console.WriteLine("Capital final : {0}", cap_fin);
            Console.ReadKey();

        }
    }
}
